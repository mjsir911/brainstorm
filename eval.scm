(define (eval? expr)
	(call-with-current-continuation
		(lambda (k)
			(with-exception-handler (lambda (x) (k #f))
			                        (lambda () (begin (eval expr) #t))))))

(define (try-eval expr)
	(if (eval? expr)
		(eval expr)
		expr))


(define (ns-eval expr ns)
	(eval `(let ,ns ,expr)))

(define (lambda-eval expr)
	(lambda (ns) (ns-eval expr ns)))

