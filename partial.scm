(include "utils.scm")
(include "eval.scm")

; -----

; you have a tree
; some end nodes are tainted
; some subtrees have the possibility of being partially or fully simplified even when tainted
; untainted subtrees can be simplified absolutely

; I have a tree
; check which nodes are tainted: eval?
; evaluate all untainted nodes: eval
; evaluate all tainted nodes with fallback: try-eval



(define (partial-eval tainteds expr)
	(if (member expr tainteds)
		(list 'quote expr)
		(if (list? expr)
			(let [(transformed (map (curry partial-eval tainteds) expr))]
				(if (equal? transformed expr) ; is expr tainted?
					(eval transformed) ; is not tainted, fully simplifiable
					(if (eval? transformed) ; is tainted, let's see if we can simplify it
						(let [(ret (eval transformed))] ; tainted simplification
							(if (list? ret)
								(list 'quote ret)
								ret))
						(map dequote transformed)))) ; un-simplifiable tainted
			expr)))
