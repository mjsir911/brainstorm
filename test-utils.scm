(define (color code)
(string-append "\033[" code "m"))
(define (red)
(color "0;31"))
(define (green)
	(color "0;32"))
(define (nc)
	"\033[0m")

(define (test expr expected)
	; (ns-eval expected '[(x 1)])
	(let [(actual (partial-eval '[x] expr))]
		(if (equal? actual expected)
			(print (green) "✓" (nc) "\t" actual)
			(print (red) "✗" (nc) "\t" actual "\t\t!=\t\t" expected))))
