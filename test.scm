(include "test-utils.scm")
(include "partial.scm")

(test '1 1)
(test 'x ''x)
(test '(+ 2 1) 3)
(test '(+ x 1) '(+ x 1))
(test '(+ x (+ 1 2)) '(+ x 3))

(test '(list x 1 2) ''(x 1 2))

(test '(reverse (list 1 2 x)) ''(x 2 1))
(test '(reverse '(1 2 x)) ''(x 2 1)) ; y fail ;-;
