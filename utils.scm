(define (dequote a)
		(if (and (list? a)
		         (equal? (car a) 'quote))
			(cadr a)
			a))

(define (curry f . out-args)
	(lambda in-args
		(apply f (append out-args in-args))))
